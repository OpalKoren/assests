sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createAssestsModel: function () {
			var oData = {
					"Assests": [{
						"Block": "6211",
						"Halaka": "643",
						"Line": "ירוק",
						"Town": "תל אביב",
						"Deleted": false
					}, {
						"Block": "6215",
						"Halaka": "642",
						"Line": "סגול",
						"Town": "חולון",
						"Deleted": true
					}, {
						"Block": "6636",
						"Halaka": "640",
						"Line": "ירוק",
						"Town": "ירושלים",
						"Deleted": true
					}]
				};

			var oModel = new JSONModel(oData);
			return oModel;
		}

	};
});