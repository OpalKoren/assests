sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Controller, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("assests.Assests.controller.Main", {
		onInit: function () {

		},

		onShowAssestsPress: function (oEvent) {
			this.byId("AssestsVbox").setVisible(true); 
			this.byId("nextButton").setEnabled(true);
			

		},

		onNextPress: function (oEvent) {
			var oWizardStep = oEvent.getSource();
			oWizardStep.setVisible(true);
			this.getView().byId('UpdateStatusWizzard').nextStep();

		},

		onBackPress: function (oEvent) {

			var oWizardStep = oEvent.getSource();
			oWizardStep.setVisible(true);
			this.getView().byId('UpdateStatusWizzard').previousStep();
		},

		handleValueChange: function (oEvent) {
			this.getView().byId('fileName').setText(oEvent.getParameter("newValue"));
		},

		onMessageSuccessDialogPress: function () {
			
			var oView = this.getView();
			var oDialog = oView.byId("sucessDialog");
			if (!oDialog) {
				oDialog = sap.ui.xmlfragment("assests.Assests.view/SucessDialog", this);
				oView.addDependent(oDialog);
			}

			oDialog.open();
		},

		onCloseDialog: function (oEvent) {
		
		}
	});
});