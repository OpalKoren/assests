/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"assests/Assests/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});